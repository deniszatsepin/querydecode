var should = require('chai').should();
var queryDecode = require('../lib/querydecoder.js').queryDecode;

describe('Query decoder', function () {

  describe('queryDecode', function () {
    var query1 = "?name=denis&age=22&skills=js&skills=html5";
    var query2 = "?person[name]=denis&person[age]=22&person[skills]=js&person[skills]=html5";

    it('should return empty object', function () {
      var res = queryDecode();
      JSON.stringify(res).should.equal('{}');
    });

    it('should return object', function () {
      var res = queryDecode(query1);
      JSON.stringify(res).should.equal('{"name":"denis","age":"22","skills":["js","html5"]}');
    });
    
    it('should return object with subobject', function () {
      var res = queryDecode(query2);
      JSON.stringify(res).should.equal('{"person":{"name":"denis","age":"22","skills":["js","html5"]}}');
    });
    
  });
});
