exports.queryDecode = function (query) {
  var result = {};
  if (typeof query !== 'string') {
    return result;
  }

  var params_str = query.substring(query.indexOf('?') + 1).replace(/\+/g, '%20');
  var params = params_str.split('&');

  if (params.length === 0) {
    return result;
  }
  var reg = /^(\w+)\[?.*$/;
  var remove = /[^\w]/g;
  for (var i = 0, len = params.length; i < len; i += 1) {
    var keyval = params[i].split('=');
    var ma = /\[([^\[]*)\]/g;
    
    if (keyval.length === 0) {
      continue;
    } 

    var key = keyval.shift();
    var value = keyval.join('=');
    value = decodeURIComponent(value);


    if (key.length === 0) {
      continue;
    }
    
    key = decodeURIComponent(key);

    var subkeys = [];
    var max = 2, m = 0;
    while((res = ma.exec(key)) !== null) {
      if (m > max) {
        break;
      }
      m += 1;
      if (typeof res[1] === 'undefined') {
        break;
      }
      var kkey = res[1].replace(remove, '');
      subkeys.push(kkey);
    }
    var components = reg.exec(key);
    var _key = components[1].replace(remove, '');

    if (_key.length === 0) {
      continue;
    }

    var current = null, skey = null;

    if (subkeys.length > 0) {
      subkeys.unshift(_key);
      current = result;
      for (var s = 0, slen = subkeys.length; s < slen - 1; s += 1) {
        skey = subkeys[s];
        if (typeof current[skey] === 'undefined') {
          current[skey] = {};
        }
        current = current[skey];
      }
      skey = subkeys[slen - 1];
    } else {
      current = result;
      skey = _key;
    } 

    if (typeof current[skey] === 'undefined') {
      current[skey] = value;
    } else if (typeof current[skey] === 'array') {
      current[skey].push(value);
    } else {
      current[skey] = [current[skey], value];
    } 

    

  }

  return result;

};
